import React, {Component, Fragment} from 'react';

class Form extends Component {
  render() {
    return (
      <Fragment>
        <input type="text" placeholder='Author' onChange={(event) => this.props.valAuthor(event)}/>
        <input type="text" placeholder='Text' onChange={(event) => this.props.valText(event)}/>
        <button onClick={this.props.sendMessage}>Send</button>
      </Fragment>
    );
  }
}

export default Form;