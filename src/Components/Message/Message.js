import React, {Component, Fragment} from 'react';
import Moment from 'react-moment';
import './Message.css'

class Message extends Component {
  render() {
    return (
      <Fragment>
        <div><span>{this.props.name}: </span><span>{this.props.text} </span> <span className='Time'><Moment format="DD-MM-YYYY HH:mm">{this.props.time}</Moment></span></div>
      </Fragment>
    );
  }
}

export default Message;