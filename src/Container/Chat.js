import React, {Component, Fragment} from 'react';
import './Chat.css'

import Message from "../Components/Message/Message";
import Form from "../Components/Message/Form/Form";

class Chat extends Component {
  state={
    messages:[],
    author: '',
    text: '',
  };


  getAuthorName = (event) => {
    this.setState({author: event.target.value})
  };

  getText = (event) => {
    this.setState({text: event.target.value})
  };

  sendMessage = () =>{
    clearInterval(this.interval);
    const url = 'http://146.185.154.90:8000/messages';
    const data = new URLSearchParams();
    data.set('message', this.state.text);
    data.set('author', this.state.author);
    fetch(url, {
      method: 'post',
      body: data,
    }).then((response)=>{
      console.log(response)
    });
  };

  getMessage = () =>{
    this.intreval = setInterval(()=>{
      let date;
      if(this.state.messages.length > 0) date = this.state.messages[this.state.messages.length - 1].datetime;
      const getAllmessagesURL = "http://146.185.154.90:8000/messages";
      const getLastMessageURL = `http://146.185.154.90:8000/messages?datetime=${date}`;
      fetch(date? getLastMessageURL : getAllmessagesURL).then(response => {
        if(response.ok) {
          return response.json();
        }
      }).then(response => {
        this.setState({messages: [...this.state.messages].concat(response)})
      })
    }, 3000);

  };
  componentDidMount(){
    this.getMessage()
  }

  render() {
    return (
      <Fragment>
        <div id='Chat'>
          {this.state.messages.map((item, index)=>{
            return(
              <div key={item._id}>
                <Message
                  index={index}
                  id={item.id}
                  name={item.author}
                  text={item.message}
                  time={item.datetime}
                />
              </div>
            )
          })}
        </div>
        <div id='Form'>
          <Form
            sendMessage={this.sendMessage}
            valAuthor={(event) => this.getAuthorName(event)}
            valText={(event)=> this.getText(event)}
          />
        </div>
      </Fragment>
    );
  }
}

export default Chat;